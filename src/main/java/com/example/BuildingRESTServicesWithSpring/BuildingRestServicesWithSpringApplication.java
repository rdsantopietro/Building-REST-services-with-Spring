package com.example.BuildingRESTServicesWithSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;


@SpringBootApplication
public class BuildingRestServicesWithSpringApplication {

    public static void main(String[]  args) {
        SpringApplication app = new SpringApplication(BuildingRestServicesWithSpringApplication.class);
        Environment env = app.run(args).getEnvironment();
    }
}