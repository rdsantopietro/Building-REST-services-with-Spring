package com.example.BuildingRESTServicesWithSpring.repository;

import com.example.BuildingRESTServicesWithSpring.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
