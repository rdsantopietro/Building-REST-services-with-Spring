package com.example.BuildingRESTServicesWithSpring.exception;

public class EmployeeAlreadyExistException extends RuntimeException {
    public EmployeeAlreadyExistException(Long id){
        super ("Already exist Empoyee with id :" + id);
    }
}
