package com.example.BuildingRESTServicesWithSpring.exception;

import com.example.BuildingRESTServicesWithSpring.exception.EmployeeAlreadyExistException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
 class EmployeeAlreadyExistAdvice {

    @ResponseBody
    @ExceptionHandler(EmployeeAlreadyExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String employeeNotFoundHandler(EmployeeAlreadyExistException ex) {
        return ex.getMessage();
    }
}