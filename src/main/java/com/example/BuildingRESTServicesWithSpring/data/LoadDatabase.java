package com.example.BuildingRESTServicesWithSpring.data;


import com.example.BuildingRESTServicesWithSpring.domain.Employee;
import com.example.BuildingRESTServicesWithSpring.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(EmployeeRepository repository) {

        return args -> {
            for (int x= 0; x< 150; x++){
                log.info("Preloading " + repository.save(new Employee("Bilbo Baggins", "burglar")));
                log.info("Preloading " + repository.save(new Employee("Frodo Baggins", "thief")));
            }
        };
    }
}
