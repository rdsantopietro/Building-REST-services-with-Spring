package com.example.BuildingRESTServicesWithSpring.rest;

import com.example.BuildingRESTServicesWithSpring.domain.Employee;
import com.example.BuildingRESTServicesWithSpring.exception.EmployeeAlreadyExistException;
import com.example.BuildingRESTServicesWithSpring.exception.EmployeeNotFoundException;
import com.example.BuildingRESTServicesWithSpring.repository.EmployeeRepository;
import javassist.NotFoundException;
import org.apache.tomcat.util.http.ResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.util.List;

import static org.apache.tomcat.util.http.ResponseUtil.*;

@RestController
public class EmpoloyeeController {

    private final EmployeeRepository employeeRepository;

    EmpoloyeeController (EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("/employees")
    ResponseEntity<List<Employee>> findAll(){
        return ResponseEntity.ok().body(employeeRepository.findAll());
    }

    @GetMapping("/employees/{id}")
    ResponseEntity<Employee> findOne(@PathVariable Long id) throws  URISyntaxException {
        Employee result  = employeeRepository.findById(id).orElseThrow(() -> new EmployeeNotFoundException(id));
        return ResponseEntity.ok().location(new URI("/employees" + result.getId())).body(result);
    }

    @PostMapping("/employees")
    ResponseEntity<Employee> save(@RequestBody Employee employee) throws URISyntaxException {
        if(employee.getId() != null){
            throw new EmployeeAlreadyExistException(employee.getId());
        }

        Employee result = employeeRepository.save(employee);
        return ResponseEntity.created(new URI("/employees/" + result.getId())).body(result);
    }

    @PutMapping("/employee/{id}")
    Employee update (@RequestBody Employee employee, @PathVariable Long id){
        return employeeRepository.findById(id)
                .map(employeex -> {
                    employeex.setName(employee.getName());
                    employeex.setRole(employee.getRole());
                    return employeeRepository.save(employeex);
                })
                .orElseGet(() -> {
                    employee.setId(id);
                    return employeeRepository.save(employee);
                });
    }

    @DeleteMapping("/employee/{id}")
    void delete (@PathVariable Long id){
        employeeRepository.deleteById(id);
    }




}
